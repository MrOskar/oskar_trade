<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Pusher\Pusher;

class HomeController extends Controller
{

    private $tradeDir;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->tradeDir = config('app.db_path');
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        if(!isset($_GET['file'])){
            $file = Auth::user()->trade_db;
        } else {
            $file = $_GET['file'];
        }

        Config::set("database.connections.trading", [
            'driver' => 'sqlite',
            "database" => $this->tradeDir . '' . $file,
        ]);



        $trades = DB::connection('trading')->select('Select * from trades where "sell_reason" IS NOT null ORDER BY close_date DESC');
        $open_trades = DB::connection('trading')->select('Select * from trades where "is_open" = 1 ORDER BY close_date DESC');
        $trades_plus = DB::connection('trading')->select('Select * from trades where "close_profit" > 0 ORDER BY close_date DESC');
        $trades_minus = DB::connection('trading')->select('Select * from trades where "close_profit" < 0 ORDER BY close_date DESC');
        $avg = $this->calculateAverage(array_column($trades, "close_profit"));

        foreach ($open_trades as $open_trade) {

            $symbol = str_replace('/', '', $open_trade->pair);
            $response = Http::get('https://api.binance.com/api/v3/ticker/price?symbol='.$symbol)->json();
            $res = $response['price'] * 100 / $open_trade->open_rate - 100;
            $open_trade->profit = number_format($res,2);

        }

        $avg = number_format($avg * 100,2);
        $response = Http::get('https://blockchain.info/ticker')->json();
        $profit_btc = array_sum(array_column($trades_plus,'close_profit_abs'));
        $profit_btc = number_format($profit_btc,6);
        $profit_usd = number_format($profit_btc * $response['USD']['15m'],2);
        return view('home', compact('trades', 'avg', 'trades_plus', 'trades_minus', 'open_trades', 'profit_btc', 'profit_usd'));

    }

    public function orders()
    {
        Config::set("database.connections.trading", [
            'driver' => 'sqlite',
            "database" => $this->tradeDir . '' . Auth::user()->trade_db,
        ]);

        $orders = DB::connection('trading')->select('Select * from orders');

        return view('orders', compact('orders'));


    }

    public function technical()
    {
        if(isset($_GET['coin'])){
            $coin = $_GET['coin'];
        } else {
            $coin = 'BINANCE:BCHBTC';
        }

        Config::set("database.connections.trading", [
            'driver' => 'sqlite',
            "database" => $this->tradeDir . '' . Auth::user()->trade_db,
        ]);

        $s_coin = str_replace('BINANCE:', '', $coin);
        $s_coin = str_replace('BTC', '/BTC', $s_coin);
        $open_trades = DB::connection('trading')
            ->select('Select * from trades where "is_open" = 1 AND "pair" = "' .$s_coin. '" ORDER BY close_date DESC');
        foreach ($open_trades as $open_trade) {

            $symbol = str_replace('/', '', $open_trade->pair);
            $response = Http::get('https://api.binance.com/api/v3/ticker/price?symbol='.$symbol)->json();
            $res = $response['price'] * 100 / $open_trade->open_rate - 100;
            $open_trade->profit = number_format($res,2);

        }
        return view('technical', compact('open_trades', 'coin'));


    }

    public function tutorials()
    {

        return view('tutorials');

    }

    public function test()
    {
        $pusher = new Pusher("215f950079c8d757b120", "25fb954f954b90bb869a", "1260646", array('cluster' => 'eu'));

        $pusher->trigger('my-channel', 'my-event', array('message' => 'hello world'));
    }

    function calculateAverage($array){
        $sum = array_sum($array);
        if($sum <= 0){
            return 0;
        }
        $average = $sum / count($array);

        return $average;
    }

    function changeFile(){
        if (isset($_POST['user_stats'])) {

            return redirect()->route('home', array('file' => $_POST['user_stats']));
        }
    }


}
