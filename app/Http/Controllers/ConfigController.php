<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ConfigController extends Controller
{

    private $tradeDir;

    public function __construct()
    {
        $this->tradeDir = config('app.db_path');
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();
        $file = file_get_contents($this->tradeDir . $user->config_file);
        $config = json_decode($file, true);

        $user_config = [

            'max_open_trades' => $config['max_open_trades'],
            'stake_currency' => $config['stake_currency'],
            'stake_amount' => $config['stake_amount'],
            'fiat_display_currency' => $config['fiat_display_currency'],
            'unfilledtimeout' => $config['unfilledtimeout']
        ];

        return route('config', compact('user_config'));

    }
}
