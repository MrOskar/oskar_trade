<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {

        $news = News::orderBy('pubDate', 'desc')->take(10)->get();

        return view('news', compact( 'news'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $coins = [
            'dogecoin',
            'ripple coin',
            'bitcoin',
            'ethereum',
            'Terra coin',
            'monero coin'
        ];

        foreach ($coins as $coin) {

            $response = Http::get('https://newsdata.io/api/1/news', [
                'apikey' => 'pub_10906bd331e3a29d23b591223af5e22f73f2',
                'q' => $coin . ' -kaufen',
                'language' => 'de'
            ]);
            $res = $response->json();

            foreach ($res['results'] as $re) {
                News::updateOrCreate(
                    ['title' => $re['title'], 'description' => $re['description']],
                    [
                        'link' => $re['link'],
                        'pubDate' => $re['pubDate'],
                        'image_url' => $re['image_url'],
                        'coin' => $coin,
                    ]
                );
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        //
    }
}
