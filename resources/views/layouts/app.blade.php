<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://js.pusher.com/beams/1.0/push-notifications-cdn.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <script src="https://js.pusher.com/7.0.3/pusher.min.js"></script>
    <script>
        var pusher = new Pusher("215f950079c8d757b120", {
            cluster: "eu",
        });
        var channel = pusher.subscribe("my-channel");

        channel.bind("my-event", (data) => {
            alert(data.message)
        });
    </script>
</head>
<body>
    <div id="app">
        <div class="wrapper">
            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                    </li>
{{--                    <li class="nav-item d-none d-sm-inline-block">--}}
{{--                        <a href="../../index3.html" class="nav-link">Home</a>--}}
{{--                    </li>--}}
                </ul>

                <!-- Right navbar links -->
                <ul class="navbar-nav ml-auto">
                    <!-- Navbar Search -->
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link" data-widget="navbar-search" href="#" role="button">--}}
{{--                            <i class="fas fa-search"></i>--}}
{{--                        </a>--}}
{{--                        <div class="navbar-search-block">--}}
{{--                            <form class="form-inline">--}}
{{--                                <div class="input-group input-group-sm">--}}
{{--                                    <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">--}}
{{--                                    <div class="input-group-append">--}}
{{--                                        <button class="btn btn-navbar" type="submit">--}}
{{--                                            <i class="fas fa-search"></i>--}}
{{--                                        </button>--}}
{{--                                        <button class="btn btn-navbar" type="button" data-widget="navbar-search">--}}
{{--                                            <i class="fas fa-times"></i>--}}
{{--                                        </button>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </form>--}}
{{--                        </div>--}}
{{--                    </li>--}}
                    <li class="nav-item">
                        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                            <i class="fas fa-expand-arrows-alt"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                            <i class="fas fa-th-large"></i>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.navbar -->

            <!-- Main Sidebar Container -->
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <!-- Brand Logo -->
                <a href="/" class="brand-link">
                    <span class="ml-4 brand-text font-weight-light">OskarTrade v0.0.1</span>
                </a>

                <!-- Sidebar -->
                <div class="sidebar">
                    <!-- Sidebar user (optional) -->
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div class="image">
                            <img src="../../dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
                        </div>
                        <div class="info d-flex">
                            <p  class="d-block text-white">{{\Illuminate\Support\Facades\Auth::user()->name}}</p>
                            <a title="Logout" class="ml-4"  href="{{route('logout')}}"><i class="fas fa-sign-out-alt"></i></a>
                        </div>
                    </div>

                    <!-- Sidebar Menu -->
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                            <!-- Add icons to the links using the .nav-icon class
                                 with font-awesome or any other icon font library -->
                            <li class="nav-item">
                                <a href="{{route('home')}}" class="nav-link">
                                    <i class="nav-icon fas fa-home"></i>
                                    <p>
                                        Dashboard
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('orders')}}" class="nav-link">
                                    <i class="nav-icon fab fa-btc"></i>
                                    <p>
                                        Orders
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/portfolio" class="nav-link">
                                    <i class="nav-icon fab fa-bitcoin"></i>
                                    <p>
                                        Bot Whitelist Coins
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/technical" class="nav-link">
                                    <i class="nav-icon fas fa-chart-line"></i>
                                    <p>
                                        Technische Analyse
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('news')}}" class="nav-link">
                                    <i class="nav-icon far fa-newspaper"></i>
                                    <p>
                                        Crypto News
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('tutorials')}}" class="nav-link">
                                    <i class="nav-icon fa fa-info"></i>
                                    <p>
                                        Tutorials
                                    </p>
                                </a>
                            </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                    <!-- /.sidebar-menu -->
                </div>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1>@yield('pageName')</h1>
                            </div>
                            <div class="col-sm-6">
{{--                                <ol class="breadcrumb float-sm-right">--}}
{{--                                    <li class="breadcrumb-item"><a href="#">Home</a></li>--}}
{{--                                    <li class="breadcrumb-item"><a href="#">Layout</a></li>--}}
{{--                                    <li class="breadcrumb-item active">Fixed Layout</li>--}}
{{--                                </ol>--}}
                            </div>
                        </div>
                    </div><!-- /.container-fluid -->
                </section>

                <!-- Main content -->
                <section class="content">
                    @yield('content')

                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <footer class="main-footer">
                <div class="float-right d-none d-sm-block">
                    <b>Version</b> 0.0.1
                </div>
                <strong>Copyright &copy; 2021 <a href="https://websoly.com">WEBsoly</a>.</strong> All rights reserved.
            </footer>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">

            </aside>
            <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->
    </div>

</body>
</html>
