@extends('layouts.app')

<!-- PAGE SETTINGS -->
@section('pageName', 'Tutorials')



@section('content')
    <div class="col-md-6" id="accordion">
        <p>Hier findest du eine reihe von Tutorials, die Dir dabei helfen deine Software einzurichten</p>
        <div class="card card-dark card-outline">
            <a class="d-block w-100" data-toggle="collapse" href="#collapseOne" aria-expanded="true">
                <div class="card-header">
                    <h4 class="card-title text-dark w-100">
                        <i class="fab fa-btc"></i> Binance Account erstellen
                    </h4>
                </div>
            </a>
            <div id="collapseOne" class="collapse" data-parent="#accordion" style="">
                <div class="card-body">
                    <a href="https://accounts.binance.com/de/register?ref=36444882">Hier</a>
                    <!-- TODO GET USER REF LINK -->
                    kannst du dir Kostenlos einen Binance Account erstellen
                </div>
            </div>
        </div>
        <div class="card card-dark card-outline">
            <a class="d-block w-100" data-toggle="collapse" href="#collapseTwo" aria-expanded="true">
                <div class="card-header">
                    <h4 class="card-title text-dark w-100">
                        <i class="fa fa-user-check"></i> Binance Account Verifizieren
                    </h4>
                </div>
            </a>
            <div id="collapseTwo" class="collapse" data-parent="#accordion" style="">
                <div class="card-body">
                    Um die Software verwenden zu können, muss dein Binance Account Verifiziert sein.<br>

                </div>
            </div>
        </div>
        <div class="card card-dark card-outline">
            <a class="d-block w-100" data-toggle="collapse" href="#collapseThree" aria-expanded="true">
                <div class="card-header">
                    <h4 class="card-title text-dark w-100">
                        <i class="fa fa-key"></i> API Key erstellen
                    </h4>
                </div>
            </a>
            <div id="collapseThree" class="collapse" data-parent="#accordion" style="">
                <div class="card-body">
                    Um die Software verwenden zu können, benötigst du API Keys von deinem Binance Konto<br>
                    Bitte Beachte Folgende einstellungen: <br><br>

                    <p><i class="fa fa-check"></i> Spot- und Margin-Handel aktivieren</p>
                    <p><i class="fa fa-times"></i> Auszahlungen aktivieren</p>

                </div>
            </div>
        </div>
    </div>
@endsection
