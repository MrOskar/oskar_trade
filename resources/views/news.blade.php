@extends('layouts.app')
<?php use Carbon\Carbon;?>
<!-- PAGE SETTINGS -->
@section('pageName', 'News')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-bullhorn"></i>
                            News & Updates
                        </h3>
                    </div>
                    <div class="card-body">
                        @foreach($news as $new)
                            <div class="callout callout-primary">
                                <h5><a href="{{$new->link}}">{{$new->title}}</a></h5>
                                <p>{{$new->description}}</p>
                                <p class="text-muted"> am: {{Carbon::parse($new->pubDate)->format('d. M  Y H:i')}}</p>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
