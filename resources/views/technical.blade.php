@extends('layouts.app')
<?php use Carbon\Carbon;?>
<!-- PAGE SETTINGS -->
@section('pageName', 'Technische Analyse')

@section('content')

    <?php

    $coins =
        [
            [
                "name" =>  "BINANCE:ALGOBTC",
            ],
            [
                "name" =>  "BINANCE:BATBTC",
            ],
            [
                "name" =>  "BINANCE:BCHBTC",
            ],
            [
                "name" =>  "BINANCE:BRDBTC",
            ],
            [
                "name" =>  "BINANCE:EOSBTC",
            ],
            [
                "name" =>  "BINANCE:ETHBTC",
            ],
            [
                "name" =>  "BINANCE:IOTABTC",
            ],
            [
                "name" =>  "BINANCE:LINKBTC",
            ],
            [
                "name" =>  "BINANCE:LTCBTC",
            ],
            [
                "name" =>  "BINANCE:NEOBTC",
            ],
            [
                "name" =>  "BINANCE:NXSBTC",
            ],
            [
                "name" =>  "BINANCE:XMRBTC",
            ],
            [
                "name" =>  "BINANCE:XRPBTC",
            ],
            [
                "name" =>  "BINANCE:XTZBTC",
            ],
            [
                "name" =>  "BINANCE:LUNABTC",
            ],
            [
                "name" =>  "BINANCE:AVAXBTC",
            ],
            [
                "name" =>  "BINANCE:ADABTC",
            ]
        ];
    ?>
    <div class="container-fluid">
        <form method="GET" action="/technical">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-6 col-md-6">

                                <select name="coin" class="form-control ml-2">
                                    @foreach($coins as $one)
                                        <option @if($coin == $one['name']) selected="" @endif value="{{$one['name']}}">{{str_replace('BINANCE:','',$one['name'])}}</option>
                                    @endforeach

                                </select>
                            </div>
                            <div class="col-6 text-left my-auto">
                                <button class="btn btn-secondary btn-sm" type="submit">Aktualisieren</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </form>
        <div class="row mt-2">
            <div class="col-12">
               <div class="row">
                   <div class="col-md-5 col-xl-3">
                       <!-- TradingView Widget BEGIN -->
                       <div class="tradingview-widget-container">
                           <div class="tradingview-widget-container__widget"></div>
                           <div class="tradingview-widget-copyright"><a href="https://de.tradingview.com/symbols/{{str_replace('BINANCE:','', $coin)}}/technicals/" rel="noopener" target="_blank"><span class="blue-text">Technische Analyse für {{ str_replace('BINANCE:','', $coin) }}</span></a> von TradingView</div>
                           <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-technical-analysis.js" async>
                               {
                                   "interval": "15m",
                                   "width": "100%",
                                   "isTransparent": false,
                                   "height": 450,
                                   "symbol": "{{$coin}}",
                                   "showIntervalTabs": true,
                                   "locale": "de_DE",
                                   "colorTheme": "light"
                               }
                           </script>
                       </div>
                       <!-- TradingView Widget END -->
                       @if($open_trades)
                       <div class="card mt-3">
                           <div class="card-header">
                               <h3 class="card-title">Offene Trades</h3>

                               <div class="card-tools">
                               </div>
                           </div>
                           <!-- /.card-header -->
                           <div class="card-body table-responsive p-0">
                               <table class="table table-hover text-nowrap">
                                   <thead>
                                   <tr>
                                       <th>OPEN</th>
                                       <th>PROFIT</th>
                                   </tr>
                                   </thead>
                                   <tbody>
                                   <tr>
                                       @foreach($open_trades as $open_trade)

                                       <td class="font-weight-bold h5">
                                           <span class="text-muted">{{number_format($open_trade->open_rate,10)}}</span>
                                       </td>
                                           @if($open_trade->profit < 0)
                                               <td class="text-danger font-weight-bold h5">{{$open_trade->profit}} %</td>
                                           @else
                                               <td class="text-success font-weight-bold h5">{{$open_trade->profit}} %</td>
                                           @endif
                                       @endforeach
                                   </tr>
                                   </tbody>
                               </table>
                           </div>
                           <!-- /.card-body -->
                       </div>
                       @endif
                   </div>
                   <div class="col-md-12 col-xl-9">
                       <!-- TradingView Widget BEGIN -->
                       <div class="tradingview-widget-container">
                           <div id="tradingview_dc2a4"></div>
                           <div class="tradingview-widget-copyright">
                               <a href="https://de.tradingview.com/symbols/{{str_replace('BINANCE:','', $coin)}}/?exchange=BINANCE" rel="noopener" target="_blank"><span class="blue-text">{{str_replace('BINANCE:','', $coin)}} Chart</span></a> von TradingView</div>
                           <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
                           <script type="text/javascript">
                               new TradingView.widget(
                                   {
                                       "width": "100%",
                                       "height": 610,
                                       "symbol": "{{$coin}}",
                                       "interval": "15",
                                       "timezone": "Etc/UTC",
                                       "theme": "light",
                                       "style": "1",
                                       "locale": "de_DE",
                                       "toolbar_bg": "#f1f3f6",
                                       "enable_publishing": false,
                                       "allow_symbol_change": false,
                                       "hide_side_toolbar" : false,
                                       "studies": [
                                           "BB@tv-basicstudies",
                                           "RSI@tv-basicstudies"
                                       ],
                                       "container_id": "tradingview_dc2a4"
                                   }
                               );
                           </script>
                       </div>
                       <!-- TradingView Widget END -->
                   </div>
               </div>
            </div>
        </div>
    </div>
@endsection
