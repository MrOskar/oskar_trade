@extends('layouts.app')
<?php use Carbon\Carbon;?>
<!-- PAGE SETTINGS -->
@section('pageName', 'Coin Whitelist')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-bullhorn"></i>
                            Whitelist
                        </h3>
                    </div>
                    <div class="card-body"><!-- TradingView Widget BEGIN -->
                        <div class="tradingview-widget-container">
                            <div class="tradingview-widget-container__widget"></div>
                            <div class="tradingview-widget-copyright"><a href="https://de.tradingview.com" rel="noopener" target="_blank"><span class="blue-text">Crypto</span></a> von TradingView</div>
                            <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-market-quotes.js" async>
                                {
                                    "width": "100%",
                                    "height": "800",
                                    "symbolsGroups": [
                                    {
                                        "name": "Crypto",
                                        "symbols": [
                                            {
                                                "name": "BINANCE:ALGOBTC",
                                                "displayName": "ALGO"
                                            },
                                            {
                                                "name": "BINANCE:BATBTC",
                                                "displayName": "BAT"
                                            },
                                            {
                                                "name": "BINANCE:BCHBTC",
                                                "displayName": "BCH"
                                            },
                                            {
                                                "name": "BINANCE:BRDBTC",
                                                "displayName": "BRD"
                                            },
                                            {
                                                "name": "BINANCE:EOSBTC",
                                                "displayName": "EOS"
                                            },
                                            {
                                                "name": "BINANCE:ETHBTC",
                                                "displayName": "ETH"
                                            },
                                            {
                                                "name": "BINANCE:IOTABTC",
                                                "displayName": "IOTA"
                                            },
                                            {
                                                "name": "BINANCE:LINKBTC",
                                                "displayName": "LINK"
                                            },
                                            {
                                                "name": "BINANCE:LTCBTC",
                                                "displayName": "LTC"
                                            },
                                            {
                                                "name": "BINANCE:NEOBTC",
                                                "displayName": "NEO"
                                            },
                                            {
                                                "name": "BINANCE:NXSBTC",
                                                "displayName": "NXS"
                                            },
                                            {
                                                "name": "BINANCE:XMRBTC",
                                                "displayName": "XMR"
                                            },
                                            {
                                                "name": "BINANCE:XRPBTC",
                                                "displayName": "XRP"
                                            },
                                            {
                                                "name": "BINANCE:XTZBTC",
                                                "displayName": "XTZ"
                                            },
                                            {
                                                "name": "BINANCE:LUNABTC",
                                                "displayName": "LUNA"
                                            },
                                            {
                                                "name": "BINANCE:AVAXBTC",
                                                "displayName": "AVAX"
                                            }
                                        ]
                                    }
                                ],
                                    "showSymbolLogo": true,
                                    "colorTheme": "light",
                                    "isTransparent": false,
                                    "locale": "de_DE"
                                }
                            </script>
                        </div>
                        <!-- TradingView Widget END -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
