@extends('layouts.app')
<?php use Carbon\Carbon;?>
<!-- PAGE SETTINGS -->
@section('pageName', 'Dashboard')




@section('content')
    @if(\Illuminate\Support\Facades\Auth::user()->email === 'biz@dominik-zyla.at' || 'krueger@websoly.com')
        <div class="row">
            <div class="col-md-12"></div>
            <div class="col-12 col-md-6">
                <!-- select -->
                <form method="POST" action="{{route('change')}}">
                    @csrf
                    <div class="form-group">
                        <div class="row">
                            <div class="col-6 col-md-6">
                                <select name="user_stats" class="form-control ml-2">
                                    <option value="/tradesv3.sqlite">Dominik</option>
                                    <option value="/tobiBTC.live.sqlite">Tobi</option>
                                    <option value="/liskaBTC.live.sqlite">Liska</option>
                                    <option value="/limaBTC.live.sqlite">Lima</option>
                                    <option value="/test_1BTC.live.sqlite">TESTSTRATEGIE 1</option>
                                </select>
                            </div>
                            <div class="col-6 text-left my-auto ">
                                <button class="btn btn-secondary btn-sm" type="submit">Aktualisieren</button>
                            </div>
                        </div>

                    </div>

                </form>
            </div>
        </div>
    @endif

    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-success elevation-1"><i class="fab fa-btc"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Mein Roi</span>
                        <span class="info-box-number">
                                            {{$avg}} %
                            <small>({{$profit_btc }} <i class="fab fa-btc"></i> | {{$profit_usd}} <i class="fa fa-dollar-sign"></i>)</small>
                </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-chevron-up"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Meine Win Trades</span>
                        <span class="info-box-number">{{count($trades_plus)}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix hidden-md-up"></div>

            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-chevron-down"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Meine Loss Trades</span>
                        <span class="info-box-number">{{count($trades_minus)}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Meine Empfehlungen</span>
                        <span class="info-box-number">0</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Offene Trades</h3>

                        <div class="card-tools">
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0">
                        <div id="mychart"></div>
                        <table class="table table-hover text-nowrap">
                            <thead>
                            <tr>
                                <th>PAIR</th>
                                <th>PROFIT</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($open_trades as $open_trade)
                                <tr>
                                    <td class="font-weight-bold h5">{{$open_trade->pair}} <br>
                                        <span class="text-success">BUY: {{number_format($open_trade->open_rate,10)}}</span>
                                    </td>
                                    @if($open_trade->profit < 0)
                                        <td class="text-danger font-weight-bold h5">{{$open_trade->profit}} %</td>
                                    @else
                                        <td class="text-success font-weight-bold h5">{{$open_trade->profit}} %</td>
                                    @endif
                                    <td>
                                        <a title="Technische Analyse" class="btn btn-outline-primary btn-sm" href="{{route('technical' , ['coin' => 'BINANCE:' . str_replace('/', '', $open_trade->pair)])}}"><i class="fa fa-chart-line"></i> </a>
                                    </td>
                                    <tr>
                                        <td colspan="3"><!-- TradingView Widget BEGIN -->
                                            <div class="tradingview-widget-container">
                                                <div id="tradingview_62fd6-{{$open_trade->id}}"></div>
                                                <div class="tradingview-widget-copyright"><a href="https://de.tradingview.com/chart/?symbol=BINANCE%3A{{str_replace('/','', $open_trade->pair)}}" rel="noopener" target="_blank"><span class="blue-text">AAPL Chart</span></a> von TradingView</div>
                                                <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
                                                <script type="text/javascript">
                                                    new TradingView.widget(
                                                        {
                                                            "autosize": true,
                                                            "symbol": "BINANCE:{{str_replace('/','', $open_trade->pair)}}",
                                                            "interval": "15",
                                                            "timezone": "Etc/UTC",
                                                            "theme": "light",
                                                            "style": "1",
                                                            "locale": "de_DE",
                                                            "toolbar_bg": "#f1f3f6",
                                                            "enable_publishing": false,
                                                            "hide_top_toolbar": true,
                                                            "hide_legend": true,
                                                            "save_image": false,
                                                            "container_id": "tradingview_62fd6-{{$open_trade->id}}"
                                                        }
                                                    );
                                                </script>
                                            </div>
                                            <!-- TradingView Widget END --></td>
                                    </tr>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>


                <!-- /.card -->
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Zuletzt abgeschlossene Trades</h3>

                        <div class="card-tools">
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap">
                            <thead>
                            <tr>
                                <th>PAIR</th>
                                <th>PROFIT</th>
                                <th>REASON</th>
                                <th>CLOSE DATE</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($trades as $trade)
                                <tr>
                                    <td>{{$trade->pair}}</td>
                                    @if($trade->close_profit < 0)
                                        <td class="text-danger font-weight-bold">{{number_format($trade->close_profit * 100, 2)}} %</td>
                                    @else
                                        <td class="text-success font-weight-bold">{{number_format($trade->close_profit * 100, 2)}} %</td>
                                    @endif
                                    <td class="text-uppercase">{{$trade->sell_reason}}</td>
                                    <td class="text-uppercase">{{Carbon::parse($trade->close_date)->format('Y-m-d')}}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>


                <!-- /.card -->
            </div>
        </div>
    </div>
@endsection
