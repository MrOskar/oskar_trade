@extends('layouts.app')

<!-- PAGE SETTINGS -->
@section('pageName', 'Orders')



@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>PAIR</th>
                                <th>SIDE</th>
                                <th>AMOUNT</th>
                                <th>STATUS</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                <tr>
                                    <td>{{$order->id}}</td>
                                    <td>{{$order->ft_pair}}</td>
                                    @if($order->side === 'buy')
                                        <td class="text-success font-weight-bold">{{$order->side}}</td>
                                    @else
                                        <td class="text-danger font-weight-bold">{{$order->side}}</td>
                                    @endif

                                    <td>{{$order->amount}}</td>
                                    <td class="text-uppercase">{{$order->status}}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>


                <!-- /.card -->
            </div>
        </div>
    </div>
@endsection
