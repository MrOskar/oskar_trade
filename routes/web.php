<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/portfolio', function () {
    return view('portfolio');
});

Route::get('/logout', function () {
    Auth::logout();
    return redirect()->route('login');
});

Auth::routes(['register' => false]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/change', [App\Http\Controllers\HomeController::class, 'changeFile'])->name('change');
Route::get('/orders', [App\Http\Controllers\HomeController::class, 'orders'])->name('orders');
Route::get('/tutorials', [App\Http\Controllers\HomeController::class, 'tutorials'])->name('tutorials');
Route::get('/test', [App\Http\Controllers\HomeController::class, 'test'])->name('test');


//NEWS
Route::get('/news', [App\Http\Controllers\NewsController::class, 'index'])->name('news');
Route::get('/technical', [App\Http\Controllers\HomeController::class, 'technical'])->name('technical');
Route::get('/get-news', [App\Http\Controllers\NewsController::class, 'create'])->name('c-news');

//CONFIG
Route::get('/config', [App\Http\Controllers\ConfigController::class, 'index'])->name('config');

